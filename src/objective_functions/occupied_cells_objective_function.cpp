/* Copyright 2015 Sanjiban Choudhury
 * occupied_cells_objective_function.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/objective_functions/occupied_cells_objective_function.h"

namespace ig_learning {

OccupiedCellsObjectiveFunction::OccupiedCellsObjectiveFunction()
: bbox_lb_(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity()),
  bbox_ub_(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity()) {
}

OccupiedCellsObjectiveFunction::OccupiedCellsObjectiveFunction(Eigen::Vector3d bbox_lb, Eigen::Vector3d bbox_ub)
: bbox_lb_(bbox_lb),
  bbox_ub_(bbox_ub) {}

double OccupiedCellsObjectiveFunction::Value(const typename State::Ptr input, const typename WorldMap::Ptr world_map) const{
  return input->GetCellCountInBBox(bbox_lb_, bbox_ub_);
}

}


