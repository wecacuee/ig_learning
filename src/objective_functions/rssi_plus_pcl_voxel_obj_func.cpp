/* Copyright 2019 Vikas Dhiman
 * rssi_plus_pcl_voxel_obj_func.cpp
 *
 *  Created on:
 *      Author: Vikas Dhiman
 */

#include <numeric>
#include "ig_learning/state/rssi_state.h"
#include "ig_learning/objective_functions/rssi_plus_pcl_voxel_obj_func.h"
#include <pcl/octree/octree.h>
#include <pcl/octree/octree_pointcloud_occupancy.h>
#include <pcl/octree/octree_impl.h>


namespace ig_learning {
double RSSIPlusPclVoxelObjFunc::Value(const typename State::Ptr state, const typename WorldMap::Ptr world_map) const {
    auto pcl_value = this->PclVoxelMatchObjectiveFunction::Value(state, world_map);
    auto rssi_state = std::dynamic_pointer_cast<const RSSIState>(state);
    auto rssi_world = std::dynamic_pointer_cast<const RSSIWorldMap>(world_map);
    unsigned int n = rssi_world->getRSSIPoses().size();
    const Eigen::Vector3d& y = rssi_state->getLastAction().view.pose().position;
    std::vector<double> rssi_rewards;
    std::transform(rssi_world->getRSSIPoses().begin(),
                   rssi_world->getRSSIPoses().end(),
                   std::back_inserter(rssi_rewards),
                   [&y, &n](const Eigen::Vector3d& x) -> double {
                     double dist_sq = (x - y).transpose()*(x -y);
                     return 1 / std::max(0.01, dist_sq) / n;
                   });
    double total_rssi_signal = std::accumulate(rssi_rewards.begin(),
                                               rssi_rewards.end(), 0);

    return pcl_value + total_rssi_signal;
  }
}

