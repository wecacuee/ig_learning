/* Copyright 2015 Sanjiban Choudhury
 * ig_heuristic_penalized_motion_policy.cpp
 *
 *  Created on: Jul 25, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/online_policies/ig_heuristic_penalized_motion_policy.h"
#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"
#include "ig_active_reconstruction_octomap/ig/occlusion_aware.hpp"
#include "ig_active_reconstruction_octomap/ig/unobserved_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_entropy.hpp"
#include "ig_active_reconstruction_octomap/ig/proximity_count.hpp"
#include "ig_active_reconstruction_octomap/ig/vasquez_gomez_area_factor.hpp"
#include "ig_active_reconstruction_octomap/ig/average_entropy.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_pcl_input.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_interface.hpp"

#include "ig_active_reconstruction_ros/param_loader.hpp"

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;
namespace ig_learning {

void IGHeuristicMotionPolicy::Compute(double time_step,
                     const State &input_state,
                     const std::set<Action> action_set,
                     Action &selected_action) const {

  State state(input_state);

  selected_action = *action_set.begin();
  double best_ig_val = std::numeric_limits<double>::lowest();
  for (auto it : action_set) {
    std::vector<double> predicted_gains;
    state.computeInformationGains(it, predicted_gains, ig_calc_config_, ig_config_);
    double ig_val = 0;
    for (std::size_t i = 0; i < predicted_gains.size(); i++)
      ig_val += weights_[i]*predicted_gains[i];

    ig_val -= (it.view.pose().position - state.getLastAction().view.pose().position).norm();

    if (ig_val > best_ig_val) {
      selected_action = it;
      best_ig_val = ig_val;
    }
  }
}


}


