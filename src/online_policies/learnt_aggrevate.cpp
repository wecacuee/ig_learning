/* Copyright 2015 Sanjiban Choudhury
 * learnt_aggrevate.cpp
 *
 *  Created on: Aug 4, 2016
 *      Author: Sanjiban Choudhury
 */


#include "ig_learning/online_policies/learnt_aggrevate.h"

namespace ig_learning {

void LearntAggrevate::Compute(double time_step,
                                    const State &state,
                                    const std::set<Action> action_set,
                                    Action &selected_action) const {

  std::vector <double> tmp;
  feature_fn_->GetFeature(state, *action_set.begin(), tmp);
  unsigned int fdim = tmp.size();

  Eigen::MatrixXd feature_point(action_set.size(), fdim);
  unsigned int id = 0;
  for (auto a : action_set) {
    std::vector<double> feature_vec;
    feature_fn_->GetFeature(state, a, feature_vec);
    for (unsigned int fidx = 0; fidx < feature_vec.size(); fidx++)
      feature_point(id, fidx) = feature_vec[fidx];
    id++;
  }

  unsigned int selected_index = model_->Predict(feature_point);
  selected_action = *std::next(action_set.begin(), selected_index);
}

}


