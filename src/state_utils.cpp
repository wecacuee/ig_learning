/* Copyright 2015 Sanjiban Choudhury
 * state_utils.cpp
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state_utils.h"
#include "tf_conversions/tf_eigen.h"
#include "ig_active_reconstruction/view_space.hpp"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "geometry_msgs/PoseArray.h"
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {
namespace state_utils {

void DisplayStateActionObservationSequence(const State &state, ros::Publisher &pub_pcl, tf::TransformBroadcaster &br, ros::Publisher &pub_poses, double dt) {
  geometry_msgs::PoseArray pose_array;
  for (std::size_t i = 0; i < state.getAction_set().size(); i++) {
    // Publish action
    movements::Pose pose = state.getAction_set()[i].view.pose();
    tf::Vector3 trans;
    tf::Quaternion quat;
    tf::vectorEigenToTF(pose.position, trans);
    tf::quaternionEigenToTF(pose.orientation, quat);
    tf::Transform transform(quat, trans);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "sensor"));
    ros::Duration(dt).sleep();

    pcl::PointCloud<pcl::PointXYZ> pcl = state.getObservation_set()[i];
    pcl.header.frame_id = "world";
    pcl.header.stamp = ros::Time::now().toSec();
    pub_pcl.publish(pcl);

    pose_array.header.frame_id = "world";
    pose_array.header.stamp = ros::Time::now();
    geometry_msgs::Pose pose_msg;
    pose_msg.position.x = trans[0];
    pose_msg.position.y = trans[1];
    pose_msg.position.z = trans[2];
    tf::quaternionTFToMsg(quat, pose_msg.orientation);
    pose_array.poses.push_back(pose_msg);
    pub_poses.publish(pose_array);
    ros::Duration(dt).sleep();
  }
}

Action GetRandomAction(const std::set<Action> &action_set){
  return *std::next(action_set.begin(), std::rand() % action_set.size());
}

bool LoadWorldMap(const ros::NodeHandle &n, typename WorldMap::Ptr world_map) {
  std::string viewspace_file_path;
  ros_tools::getExpParam(viewspace_file_path,"viewspace_file_path", n);

  std::string pcd_folder_path;
  ros_tools::getExpParam(pcd_folder_path,"pcd_folder", n);

  std::string model_filename;
  ros_tools::getExpParam(model_filename,"model_filename", n);

  double model_res;
  ros_tools::getExpParam(model_res,"model_res", n);

  world_map->LoadWorldMap(viewspace_file_path, pcd_folder_path, model_filename, model_res);
  return true;
}


}
}


