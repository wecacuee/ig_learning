/* Copyright 2015 Sanjiban Choudhury
 * tsp_solver.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/offline_solvers/tsp_solver.h"
#include "ig_learning/offline_solvers/LKMatrix.h"

namespace ig_learning {
namespace tsp_solver {

double GetTourCost (const State &input_state) {
  std::vector < std::vector <double> > edge_distance = GetEdgeDistance(input_state);
  LKMatrix solver(edge_distance);
  solver.optimizeTour();
  std::vector<int> tour = solver.getCurrentTour();
  double dist = 0;
  for (unsigned int i = 0; i < tour.size()-1; i++)
    dist += edge_distance[tour[i]][tour[i+1]];
  return dist;
}

void GetOptimizedTour (const State &input_state, State &output_state) {
  std::vector < std::vector <double> > edge_distance = GetEdgeDistance(input_state);
  LKMatrix solver(edge_distance);
  solver.optimizeTour();
  std::vector<int> tour = solver.getCurrentTour();

  output_state = input_state;
  output_state.gatherActionSet(input_state.getAction_set(), tour);
  output_state.gatherObservationSet(input_state.getObservation_set(), tour);
}

std::vector < std::vector <double> > GetEdgeDistance(const State &state) {
  std::vector < std::vector <double> >  edge_distance(state.getAction_set().size(),
                                                      std::vector <double>(state.getAction_set().size(), 0.0));

  for (unsigned int i = 0; i < state.getAction_set().size(); i++) {
    for (unsigned int j = 0; j < state.getAction_set().size(); j++) {
      edge_distance[i][j] = (state.getAction_set()[i].view.pose().position -
                             state.getAction_set()[j].view.pose().position).norm();
      edge_distance[j][i] = edge_distance[i][j];
    }
  }

  return edge_distance;
}

}
}


