/* Copyright 2015 Sanjiban Choudhury
 * generalized_cost_benefit.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/offline_solvers/tsp_solver.h"

namespace ig_learning {

  template <>
double GeneralizedCostBenefit::MotionAndTourCost(const State &state,
                                                 const OfflineSolverParams &input) const{
  double old_cost = input.cost_fn->Cost(*input.state0, *input.world_map);
  State inc;
  inc.setAction_set(std::vector<Action>(state.getAction_set().begin() +
                                        (input.state0->getAction_set().size() - 1),
                                        state.getAction_set().end()));
  return old_cost + tsp_solver::GetTourCost(inc);
}

  template <>
void GeneralizedCostBenefit::RearrangeActionsOptimalTour(const State &state,
                                                         const OfflineSolverParams &input,
                                                         State &output) const{
  State inc, out_inc;
  inc.setAction_set(std::vector<Action>(state.getAction_set().begin() +
                                        (input.state0->getAction_set().size() - 1),
                                        state.getAction_set().end()));
  inc.setObservation_set(std::vector<Observation>(state.getObservation_set().begin() +
                                                 (input.state0->getObservation_set().size() - 1),
                                                  state.getObservation_set().end()));

  tsp_solver::GetOptimizedTour(inc, out_inc);
  output = *input.state0;
  for (unsigned int i = 1; i < out_inc.getAction_set().size(); i++) {
    output.appendAction(out_inc.getAction_set()[i]);
    output.appendObservation(out_inc.getObservation_set()[i]);
  }
}

  template <>
void GeneralizedCostBenefit::RecomputeObjectiveCost(const State &state,
                                                    const OfflineSolverParams &input,
                                                    OfflineSolverOutput &output) const {
  for (unsigned int i = input.state0->getAction_set().size(); i <= state.getAction_set().size(); i++) {
    State inc;
    inc.setAction_set(std::vector<Action>(state.getAction_set().begin(),
                                          state.getAction_set().begin() + i));
    inc.setObservation_set(std::vector<Observation>(state.getObservation_set().begin(),
                                                    state.getObservation_set().begin() + i));
    output.objective_trajectory.push_back(input.obj_fn->Value(std::make_shared<State>(inc),
                                                              input.world_map));
    output.cost_trajectory.push_back(input.cost_fn->Cost(inc,
                                                         *input.world_map));
  }
}

  template <>
void GeneralizedCostBenefit::Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const {
  State state(*input.state0);
  State start_state(state);
  double val = input.obj_fn->Value(input.state0, input.world_map);
  double curr_cost = input.cost_fn->Cost(state, *input.world_map);

  std::map< Action, double > marginal_gain_set, cost_gain_set, ratio_set;
  auto cmp_inc = [](std::pair<Action, double> const & a, std::pair<Action, double> const & b) {
    return a.second < b.second;
  };

  bool recompute = true;
  std::set<Action> action_set = input.world_map->getAction_set();
  unsigned counter = 0;
  while (true) {
    if (recompute) {
      recompute = false;
      marginal_gain_set.clear();
      cost_gain_set.clear();
      ratio_set.clear();

      unsigned int count = 0;
      for (auto it : action_set) {
        State state_new;
        input.state_transition->UpdateState(state, it, *input.world_map, state_new);
        double marginal_gain = input.obj_fn->Value(std::make_shared<State>(state_new),
                                                   input.world_map) - val;
        std::cerr << "Computing MotionAndTourCost on (" << count++ << "); Size: "
                  << state_new.getAction_set().size() << std::endl;
        double cost_gain = MotionAndTourCost(state_new, input) - curr_cost;
        double ratio = marginal_gain / std::max(std::numeric_limits<double>::epsilon(), cost_gain);

        marginal_gain_set[it] = marginal_gain;
        cost_gain_set[it] = cost_gain;
        ratio_set[it] = ratio;
      }
    }

    auto ratio_max = std::max_element(ratio_set.begin(), ratio_set.end(), cmp_inc);
    Action best_action = ratio_max->first;

    if (counter < input.budget &&
        curr_cost + cost_gain_set[best_action] < input.cost_budget &&
        marginal_gain_set[best_action] > 0) {
      input.state_transition->UpdateState(state, best_action, *input.world_map, state);
      val += marginal_gain_set[best_action];
      curr_cost += cost_gain_set[best_action];
      recompute = true;
      counter++;
     // ROS_ERROR_STREAM("val "<<val <<" curr "<<curr_cost);
    }

    action_set.erase(best_action);
    marginal_gain_set.erase(best_action);
    cost_gain_set.erase(best_action);
    ratio_set.erase(best_action);
    if (action_set.size() == 0 || counter >= input.budget)
      break;
  }
  State state_final(*output.state_final);
  RearrangeActionsOptimalTour(state, input, state_final);
  RecomputeObjectiveCost(state_final, input, output);
  output.action_sequence = output.state_final->getAction_set();
}


}

