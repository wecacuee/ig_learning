/* Copyright 2015 Sanjiban Choudhury
 * generalized_cost_benefit.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#include <iterator>
#include <iostream>
#include "ig_learning/offline_solvers/generalized_cost_benefit.h"
#include "ig_learning/offline_solvers/tsp_solver.h"

namespace ig_learning {


  template <>
  double RSSIGeneralizedCostBenefit::MotionAndTourCost(const RSSIState &state,
                                                       const OfflineSolverParams &input) const{
    double old_cost = input.cost_fn->Cost(*input.state0, *input.world_map);
    State inc;
    auto begin = state.getAction_set().begin() + (input.state0->getAction_set().size() - 1);
    auto end_it = state.getAction_set().end();
    std::cerr << "begin - end " << std::distance(begin, end_it) << std::endl;
    inc.setAction_set(std::vector<Action>(begin, end_it));
    std::cerr << "inc.action_set.size() " << inc.action_set.size() << std::endl;
    return old_cost + tsp_solver::GetTourCost(inc);
  }


  template <>
void RSSIGeneralizedCostBenefit::RearrangeActionsOptimalTour(const RSSIState &state,
                                                         const OfflineSolverParams &input,
                                                         RSSIState &output) const{
  State inc, out_inc;
  inc.setAction_set(std::vector<Action>(state.getAction_set().begin() +
                                        (input.state0->getAction_set().size() - 1),
                                        state.getAction_set().end()));
  inc.observation_set = std::vector<Observation>(state.getObservation_set().begin() +
                                                 (input.state0->getObservation_set().size() - 1),
                                                 state.getObservation_set().end());

  tsp_solver::GetOptimizedTour(inc, out_inc);
  output = *input.state0;
  for (unsigned int i = 1; i < out_inc.getAction_set().size(); i++) {
    output.getAction_set().push_back(out_inc.getAction_set()[i]);
    output.getObservation_set().push_back(out_inc.getObservation_set[i]);
  }
}


  template <>
  void RSSIGeneralizedCostBenefit::RecomputeObjectiveCost(const RSSIState &state,
                                                          const OfflineSolverParams &input,
                                                          RSSIOfflineSolverOutput &output) const {
    for (unsigned int i = input.state0->getAction_set().size(); i <= state.getAction_set().size(); i++) {
      State inc;
      inc.action_set = std::vector<Action>(State(state).action_set.begin(),
                                           State(state).action_set.begin() + i);
      inc.observation_set = std::vector<Observation>(State(state).observation_set.begin(),
                                                     State(state).observation_set.begin() + i);
      output.objective_trajectory.push_back(input.obj_fn->Value(inc, input.world_map));
      output.cost_trajectory.push_back(input.cost_fn->Cost(*inc, *input.world_map));
    }
  }


  template <>
void RSSIGeneralizedCostBenefit::Solve(const OfflineSolverParams &input, RSSIOfflineSolverOutput &output) const {
  State state(*input.state0);
  State start_state(state);
  double val = input.obj_fn->Value(state, input.world_map);
  double curr_cost = input.cost_fn->Cost(*state, *input.world_map);

  std::map< Action, double > marginal_gain_set, cost_gain_set, ratio_set;
  auto cmp_inc = [](std::pair<Action, double> const & a, std::pair<Action, double> const & b) {
    return a.second < b.second;
  };

  bool recompute = true;
  std::set<Action> action_set = input.world_map->getAction_set();
  unsigned counter = 0;
  while (true) {
    if (recompute) {
      recompute = false;
      marginal_gain_set.clear();
      cost_gain_set.clear();
      ratio_set.clear();

      unsigned int count = 0;
      for (auto it : action_set) {
        State state_new;
        input.state_transition->UpdateState(state, it, *input.world_map, state_new);
        double marginal_gain = input.obj_fn->Value(state_new, input.world_map) - val;
        std::cerr << "Computing MotionAndTourCost on (" << count++ << "); Size: " << State(state_new).action_set.size() << std::endl;

        // std::transform(State(state_new).action_set.begin(), State(state_new).action_set.end(),
        //                std::ostream_iterator<unsigned int>(std::cerr, " "),
        //                [](Action& a) -> unsigned int {
        //                  return a.id;
        //                });
        double cost_gain = MotionAndTourCost(state_new, input) - curr_cost;
        double ratio = marginal_gain / std::max(std::numeric_limits<double>::epsilon(), cost_gain);

        marginal_gain_set[it] = marginal_gain;
        cost_gain_set[it] = cost_gain;
        ratio_set[it] = ratio;
      }
    }

    auto ratio_max = std::max_element(ratio_set.begin(), ratio_set.end(), cmp_inc);
    Action best_action = ratio_max->first;

    if (counter < input.budget &&
        curr_cost + cost_gain_set[best_action] < input.cost_budget &&
        marginal_gain_set[best_action] > 0) {
      input.state_transition->UpdateState(state, best_action, *input.world_map, state);
      val += marginal_gain_set[best_action];
      curr_cost += cost_gain_set[best_action];
      recompute = true;
      counter++;
     // ROS_ERROR_STREAM("val "<<val <<" curr "<<curr_cost);
    }

    action_set.erase(best_action);
    marginal_gain_set.erase(best_action);
    cost_gain_set.erase(best_action);
    ratio_set.erase(best_action);
    if (action_set.size() == 0 || counter >= input.budget)
      break;
  }
  RearrangeActionsOptimalTour(state, input, output.state_final);
  RecomputeObjectiveCost(output.state_final, input, output);
  output.action_sequence = State(output.state_final).action_set;
}


}

