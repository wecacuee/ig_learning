/* Copyright 2015 Sanjiban Choudhury
 * state.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include "ig_active_reconstruction_octomap/ig/occlusion_aware.hpp"
#include "ig_active_reconstruction_octomap/ig/unobserved_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_voxel.hpp"
#include "ig_active_reconstruction_octomap/ig/rear_side_entropy.hpp"
#include "ig_active_reconstruction_octomap/ig/proximity_count.hpp"
#include "ig_active_reconstruction_octomap/ig/vasquez_gomez_area_factor.hpp"
#include "ig_active_reconstruction_octomap/ig/average_entropy.hpp"
#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_pcl_input.hpp"
#include "ig_active_reconstruction_octomap/octomap_ros_interface.hpp"

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {

bool State::InitializeState(const ros::NodeHandle &n) {
  typedef Belief::TreeType TreeType;
  TreeType::Config octree_config;
  ros_tools::getExpParam(octree_config.resolution_m,"resolution_m");
  ros_tools::getExpParam(octree_config.occupancy_threshold,"occupancy_threshold");
  ros_tools::getExpParam(octree_config.hit_probability,"hit_probability");
  ros_tools::getExpParam(octree_config.miss_probability,"miss_probability");
  ros_tools::getExpParam(octree_config.clamping_threshold_min,"clamping_threshold_min");
  ros_tools::getExpParam(octree_config.clamping_threshold_max,"clamping_threshold_max");
  belief = Belief(octree_config);
  return true;
}

visualization_msgs::MarkerArray State::VisualizeBelief() const {
  typedef Belief::TreeType TreeType;
  BeliefInterface<TreeType>::Config config;
  config.world_frame_name = "world";
  typename BeliefInterface<TreeType>::Ptr obj = const_cast<Belief&>(belief).getLinkedObj<BeliefInterface>(config);
  return obj->GetMarkerArray();
}

pcl::PointCloud<pcl::PointXYZ> State::VisualizeObservation() const {
  pcl::PointCloud<pcl::PointXYZ> cum_cloud;
  for (auto it: observation_set)
    cum_cloud += it;
  cum_cloud.header.frame_id = "world";
  cum_cloud.header.stamp = ros::Time::now().toSec();
  return cum_cloud;
}

const std::vector<Action>& State::getAction_set() const
{
    return action_set;
}

void State::setAction_set(const std::vector<Action> &value)
{
    action_set = value;
}

  void State::gatherActionSet(const std::vector<Action> &source, const std::vector<int>& indices)
{
  for (const auto&  ind: indices)
    action_set[ind] = source[ind];
}

const std::vector<Observation>& State::getObservation_set() const
{
    return observation_set;
}

void State::setObservation_set(const std::vector<Observation> &value)
{
    observation_set = value;
}

  void State::gatherObservationSet(const std::vector<Observation> &source, const std::vector<int>& indices) {
  for (const auto&  ind: indices)
    observation_set[ind] = source[ind];
}

const Belief& State::getBelief() const
{
    return belief;
}

void State::setBelief(const Belief &value)
{
    belief = value;
}

void State::computeInformationGains(const Action &action, std::vector<double> &feature,
                                    BasicRayIgCalculator<IgTreeWorldRepresentation::TreeType>::Config ig_calc_config,
                                    InformationGain<IgTreeWorldRepresentation::TreeType>::Config ig_config) {
    typedef IgTreeWorldRepresentation::TreeType TreeType;
    typename BasicRayIgCalculator<IgTreeWorldRepresentation::TreeType>::Ptr ig_calculator =
            belief.getLinkedObj<BasicRayIgCalculator >(ig_calc_config);

    ig_calculator->registerInformationGain<OcclusionAwareIg >(ig_config);
    ig_calculator->registerInformationGain<UnobservedVoxelIg >(ig_config);
    ig_calculator->registerInformationGain<RearSideVoxelIg >(ig_config);
    ig_calculator->registerInformationGain<RearSideEntropyIg >(ig_config);
    ig_calculator->registerInformationGain<ProximityCountIg >(ig_config);
    ig_calculator->registerInformationGain<VasquezGomezAreaFactorIg >(ig_config);
    ig_calculator->registerInformationGain<AverageEntropyIg >(ig_config);

    iar::world_representation::CommunicationInterface::ViewIgResult information_gains;
    iar::world_representation::CommunicationInterface::IgRetrievalCommand command;
    double ig_val = 0;

    command.path.clear();
    command.path.push_back( action.view.pose() );
    command.metric_names.push_back("OcclusionAwareIg");
    command.metric_names.push_back("UnobservedVoxelIg");
    command.metric_names.push_back("RearSideVoxelIg");
    command.metric_names.push_back("RearSideEntropyIg");
    command.metric_names.push_back("ProximityCountIg");
    command.metric_names.push_back("VasquezGomezAreaFactorIg");
    command.metric_names.push_back("AverageEntropyIg");

    ig_calculator->computeViewIg(command, information_gains);

    for (std::size_t i = 0; i < information_gains.size(); i++)
      feature.push_back(information_gains[i].predicted_gain);
}

double State::GetCellCountInBBox(const Eigen::Vector3d& bbox_lb, const Eigen::Vector3d& bbox_ub) const {
  typedef Belief::TreeType TreeType;
  BeliefInterface<TreeType>::Config config;
  config.world_frame_name = "world";
  typename BeliefInterface<TreeType>::Ptr obj = const_cast<Belief&>(belief).getLinkedObj<BeliefInterface>(config);
  return obj->GetCellCountInBBox(bbox_lb, bbox_ub);
}

Eigen::Transform<double,3,Eigen::Affine> State::addActionAndPointCloud(const pcl::PointCloud<pcl::PointXYZ>&
                                    cloud, const Action& action) {
  pcl::PointCloud<pcl::PointXYZ> cloud_tr;
  Eigen::Transform<double,3,Eigen::Affine> sensor_to_world_transform;
  sensor_to_world_transform.fromPositionOrientationScale(action.view.pose().position, action.view.pose().orientation, Eigen::Vector3d::Ones());
  pcl::transformPointCloud(cloud, cloud_tr, sensor_to_world_transform);
  action_set.push_back(action);
  observation_set.push_back(cloud_tr);
  return sensor_to_world_transform;
}

pcl::PointCloud<pcl::PointXYZ>& WorldMap::getModel_pcl()
{
    return model_pcl;
}

void WorldMap::setModel_pcl(const pcl::PointCloud<pcl::PointXYZ> &value)
{
    model_pcl = value;
}

double WorldMap::getRes() const
{
    return res;
}

void WorldMap::setRes(double value)
{
    res = value;
}

std::map<unsigned int, pcl::PointCloud<pcl::PointXYZ> > WorldMap::getPcl_lookup() const
{
    return pcl_lookup;
}

const pcl::PointCloud<pcl::PointXYZ>& WorldMap::getPointCloud(unsigned int idx) const {
  return pcl_lookup.at(idx);
}

void WorldMap::setPcl_lookup(const std::map<unsigned int, pcl::PointCloud<pcl::PointXYZ> > &value)
{
    pcl_lookup = value;
}

std::set<Action> WorldMap::getAction_set() const
{
    return action_set;
}

void WorldMap::setAction_set(const std::set<Action> &value)
{
    action_set = value;
}

bool WorldMap::LoadWorldMap(std::string view_space_filename, std::string pcd_foldername, std::string model_filename, double res) {
  iar::views::ViewSpace view_space;
  view_space.loadFromFile(view_space_filename);
  if (view_space.size() == 0)
    return false;
  unsigned int id = 0;
  action_set.clear();
  for (iar::views::ViewSpace::Iterator it = view_space.begin(); it != view_space.end(); ++it, id++) {
    Action action;
    action.view = *it;
    action.id = id;
    action_set.insert(action);
  }
  pcl_lookup.clear();
  for (unsigned i = 0; i < action_set.size(); i++) {
    std::stringstream ss;
    ss << pcd_foldername << i << ".pcd";
    pcl::PointCloud<pcl::PointXYZ> cloud;
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (ss.str(), cloud) == -1) {
      return false;
    }
    pcl_lookup[i] = cloud;
  }
  if (pcl::io::loadPCDFile<pcl::PointXYZ> (model_filename, model_pcl) == -1) {
    ROS_ERROR_STREAM("Couldnt load model pcd");
    return false;
  }
  res = res;
  return true;
}


}


