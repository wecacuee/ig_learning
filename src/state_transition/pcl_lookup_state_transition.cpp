/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.cpp
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/state_transition/pcl_lookup_state_transition.h"
#include "ig_active_reconstruction_ros/param_loader.hpp"
#include "ig_active_reconstruction/view_space.hpp"
#include <pcl/common/transforms.h>

namespace iar = ig_active_reconstruction;
using namespace iar::world_representation::octomap;

namespace ig_learning {

template <>
  bool PclLookupStateTransition::UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const{
  output = input;
  pcl::PointCloud<pcl::PointXYZ> cloud, cloud_tr;
  try {
    cloud = world_map.getPointCloud(action.id); // Should exist by construction
  } catch (const std::out_of_range& e) {
    ROS_ERROR_STREAM("No pcl in lookup");
    return false;
  }
  output.addActionAndPointCloud(cloud, action);
  return true;
}

template <>
  bool RSSIPclLookupStateTransition::UpdateState(const RSSIState &input, const Action &action, const RSSIWorldMap &world_map, RSSIState &rssi_output) const{
  PclLookupStateTransition transition;
  transition.UpdateState(input, action, world_map, rssi_output);
}

}



