/* Copyright 2015 Sanjiban Choudhury
 * rdf_regression_cs_classification.cpp
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/cs_classification/rdf_regression_cs_classification.h"
#include "alglib/alglib_utils.h"
#include <algorithm>
#include <iostream>
#include <fstream>

using namespace ca;

namespace ig_learning {

namespace au = alglib_utils;

RDFRegression::RDFRegression()
: CSClassification(),
  rdf_(),
  number_trees_(0) {}

bool RDFRegression::Initialize(unsigned int number_trees) {
  number_trees_ = number_trees;
  return true;
}

bool RDFRegression::Train(const std::vector<Eigen::MatrixXd> &feature_table, const std::vector<Eigen::VectorXd> &qval_table) {
  rdf_ = alglib::decisionforest();
  unsigned int rc = 0, cc = 0;
  for (auto it : feature_table) {
    rc += it.rows();
    cc = it.cols();
  }
  Eigen::MatrixXd mega_feature_set(rc, cc + 1);
  unsigned int count = 0;
  for (std::size_t i = 0; i < feature_table.size(); i++) {
    Eigen::MatrixXd feature = feature_table[i];
    Eigen::VectorXd qvals = qval_table[i];
    mega_feature_set.block(count, 0, feature.rows(), cc) = feature;
    mega_feature_set.block(count, cc, feature.rows(), 1) = qvals;
    count += feature.rows();
  }

  int num_datapoints = mega_feature_set.rows();
  int num_var = mega_feature_set.cols() - 1;

  alglib::real_2d_array train_data_ag;
  au::ConvertEigenMatToAlgLib(mega_feature_set, train_data_ag);

  alglib::dfreport rep;
  alglib::ae_int_t info;
  alglib::dfbuildrandomdecisionforest(train_data_ag, num_datapoints, num_var, 1, number_trees_, 0.2, info, rdf_, rep);

  for (std::size_t i = 0; i < mega_feature_set.rows(); i++) {
    alglib::real_1d_array feature_vec_ag;
    au::ConvertEigenVecToAlgLib(mega_feature_set.block(i, 0, 1, mega_feature_set.cols()-1).transpose(), feature_vec_ag);
    alglib::real_1d_array qval;
    alglib::dfprocess(rdf_, feature_vec_ag, qval);
  }

  return true;
}

unsigned int RDFRegression::Predict(const Eigen::MatrixXd &feature) {
  double chosen_qval = std::numeric_limits<double>::lowest();
  unsigned int chosen_class = 0;
  for (std::size_t i = 0; i < feature.rows(); i++) {
    alglib::real_1d_array feature_vec_ag;
    au::ConvertEigenVecToAlgLib(feature.row(i).transpose(), feature_vec_ag);
    alglib::real_1d_array qval;
    alglib::dfprocess(rdf_, feature_vec_ag, qval);
    if (qval[0] > chosen_qval) {
      chosen_qval = qval[0];
      chosen_class = i;
    }
  }
  return chosen_class;
}

bool RDFRegression::Save(const std::string &filename) {
  std::string serialized;
  alglib::dfserialize(rdf_, serialized);
  std::ofstream ofile(filename);
  ofile << serialized;
  ofile.close();
  return true;
}

bool RDFRegression::Load(const std::string &filename) {
  std::ifstream ifile(filename);
  alglib::decisionforest rdf;
  if (ifile) {
    std::stringstream buffer;
    buffer << ifile.rdbuf();
    std::string serialized = buffer.str();
    alglib::dfunserialize(serialized, rdf_);
  } else {
    return false;
  }
  number_trees_ = -1;
  return true;
}


}

