/* Copyright 2015 Sanjiban Choudhury
 * test_cs_classification.cpp
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#include "ig_learning/cs_classification/rdf_regression_cs_classification.h"
#include <random>
#include <iostream>

using namespace ig_learning;

int main(int argc, char **argv) {
  RDFRegression rdf;
  rdf.Initialize(20);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0, 1);

  auto func = [](const Eigen::VectorXd &f) { return f[0] - f[1]*f[1] + sin(10*f[2]); };

  unsigned int num_class = 20;
  unsigned int num_train = 1000;
  unsigned int num_test = 100;

  std::vector <Eigen::MatrixXd> feature_table;
  std::vector <Eigen::VectorXd> qval_table;
  for (unsigned int i = 0; i < num_train; i++) {
    Eigen::MatrixXd feature(num_class, 3);
    Eigen::VectorXd qval(num_class);
    for (unsigned int j = 0; j < num_class; j++) {
      for (unsigned int k = 0; k < 3; k++)
        feature(j, k) = dis(gen);
      qval[j] = func(feature.row(j).transpose());
    }
    feature_table.push_back(feature);
    qval_table.push_back(qval);
  }

  rdf.Train(feature_table, qval_table);

  rdf.Save("sample_model");

  rdf = RDFRegression();
  rdf.Load("sample_model");

  for (unsigned int i = 0; i < num_train; i++) {
    Eigen::MatrixXd feature(num_class, 3);
    Eigen::VectorXd qval(num_class);
    for (unsigned int j = 0; j < num_class; j++) {
      for (unsigned int k = 0; k < 3; k++)
        feature(j, k) = dis(gen);
      qval[j] = func(feature.row(j).transpose());
    }
    unsigned int id = rdf.Predict(feature);
    std::cout << "Selected: "<<id<<" Val: "<<qval[id]<<" Max: "<<qval.maxCoeff()<<" Min: "<<qval.minCoeff()<<std::endl;
  }
}



