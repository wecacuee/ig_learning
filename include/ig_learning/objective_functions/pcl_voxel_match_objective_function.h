/* Copyright 2015 Sanjiban Choudhury
 * pcl_voxel_match_objective_function.h
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_


#include "ig_learning/objective_function.h"

namespace ig_learning {

template <typename State, typename WorldMap>
class PclVoxelMatchObjectiveFunctionT : public ObjectiveFunctionT<State, WorldMap> {
 public:
  PclVoxelMatchObjectiveFunctionT() {}

  virtual ~PclVoxelMatchObjectiveFunctionT() {}

  virtual double Value(const typename State::Ptr state, const typename WorldMap::Ptr world_map) const;
 protected:
};

template <typename State, typename WorldMap>
using PclVoxelMatchObjectiveFunctionPtrT = boost::shared_ptr<PclVoxelMatchObjectiveFunctionT<State, WorldMap> > ;

typedef PclVoxelMatchObjectiveFunctionT<State, WorldMap> PclVoxelMatchObjectiveFunction;
typedef PclVoxelMatchObjectiveFunctionPtrT<State, WorldMap> PclVoxelMatchObjectiveFunctionPtr;
}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_PCL_VOXEL_MATCH_OBJECTIVE_FUNCTION_H_ */
