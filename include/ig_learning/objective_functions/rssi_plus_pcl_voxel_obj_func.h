/* Copyright 2015 Sanjiban Choudhury
 * pcl_voxel_match_objective_function.h
 *
 *  Created on: Jul 5, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_RSSI_PLUS_PCL_VOXEL_OBJ_FUNC_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_RSSI_PLUS_PCL_VOXEL_OBJ_FUNC_H_


#include <memory>
#include "ig_learning/state/rssi_state.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/objective_functions/pcl_voxel_match_objective_function.h"

namespace ig_learning {

class RSSIPlusPclVoxelObjFunc : PclVoxelMatchObjectiveFunction {
  public:
    virtual double Value(const typename State::Ptr state, const typename WorldMap::Ptr world_map) const;
};

typedef std::shared_ptr<RSSIPlusPclVoxelObjFunc> RSSIPlusPclVoxelObjFuncPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_DOMAIN_SPECIFIC_FUNCTIONS_RSSI_PLUS_PCL_VOXEL_OBJ_FUNC_H_ */
