/* Copyright 2015 Sanjiban Choudhury
 * feature_extractor.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_H_

#include "ig_learning/state.h"

namespace ig_learning {

class FeatureExtractor {
 public:
  FeatureExtractor() {}
  virtual ~FeatureExtractor() {}

  virtual void GetFeature(const State &state, const Action &action, std::vector<double> &feature) const = 0;
};

typedef boost::shared_ptr<FeatureExtractor> FeatureExtractorPtr;


class ConcatenateFeatureExtractor : public FeatureExtractor{
 public:
  ConcatenateFeatureExtractor()
 : feature_extractor_set_() {}

  virtual ~ConcatenateFeatureExtractor() {}

  void AddFeatureExtractorSet(const FeatureExtractorPtr &add_feat_ptr) {feature_extractor_set_.push_back(add_feat_ptr);}

  virtual void GetFeature(const State &state, const Action &action, std::vector<double> &feature) const {
    feature.clear();
    for (auto it : feature_extractor_set_) {
      std::vector<double> feature_pt;
      it->GetFeature(state, action, feature_pt);
      feature.insert(feature.end(), feature_pt.begin(), feature_pt.end());
    }
  }
 protected:
  std::vector<FeatureExtractorPtr> feature_extractor_set_;
};
typedef boost::shared_ptr<ConcatenateFeatureExtractor> ConcatenateFeatureExtractorPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_H_ */
