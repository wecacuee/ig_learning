/* Copyright 2015 Sanjiban Choudhury
 * learnt_forward_training.h
 *
 *  Created on: Jul 13, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_FORWARD_TRAINING_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_FORWARD_TRAINING_H_

#include "ig_learning/online_policy.h"
#include "ig_learning/cs_classification.h"
#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class LearntForwardTraining : public OnlinePolicy {
 public:
  LearntForwardTraining(const FeatureExtractorPtr &feature_fn,
                        const std::vector<CSClassificationPtr> &model_set)
 : feature_fn_(feature_fn),
   model_set_(model_set) {}

  virtual ~LearntForwardTraining() {}

  virtual void Compute(double time_step,
                       const State &state,
                       const std::set<Action> action_set,
                       Action &selected_action) const;
 protected:
  FeatureExtractorPtr feature_fn_;
  std::vector<CSClassificationPtr> model_set_;
};

typedef boost::shared_ptr<LearntForwardTraining> LearntForwardTrainingPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_FORWARD_TRAINING_H_ */
