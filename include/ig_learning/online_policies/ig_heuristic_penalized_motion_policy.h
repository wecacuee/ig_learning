/* Copyright 2015 Sanjiban Choudhury
 * ig_heuristic_penalized_motion_policy.h
 *
 *  Created on: Jul 25, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_PENALIZED_MOTION_POLICY_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_PENALIZED_MOTION_POLICY_H_

#include "ig_learning/online_policies/ig_heuristic_policy.h"

namespace ig_learning {

class IGHeuristicMotionPolicy : public IGHeuristicPolicy {
 public:
  IGHeuristicMotionPolicy()
  : IGHeuristicPolicy() {}

  virtual ~IGHeuristicMotionPolicy() {}

  virtual void Compute(double time_step,
                       const State &state,
                       const std::set<Action> action_set,
                       Action &selected_action) const;
};

typedef boost::shared_ptr<IGHeuristicMotionPolicy> IGHeuristicMotionPolicyPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_IG_HEURISTIC_PENALIZED_MOTION_POLICY_H_ */
