/* Copyright 2015 Sanjiban Choudhury
 * learnt_aggrevate.h
 *
 *  Created on: Aug 4, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_AGGREVATE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_AGGREVATE_H_

#include "ig_learning/online_policy.h"
#include "ig_learning/cs_classification.h"
#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class LearntAggrevate : public OnlinePolicy {
 public:
  LearntAggrevate(const FeatureExtractorPtr &feature_fn,
                        const CSClassificationPtr &model)
 : feature_fn_(feature_fn),
   model_(model) {}

  virtual ~LearntAggrevate() {}

  virtual void Compute(double time_step,
                       const State &state,
                       const std::set<Action> action_set,
                       Action &selected_action) const;
 protected:
  FeatureExtractorPtr feature_fn_;
  CSClassificationPtr model_;
};

typedef boost::shared_ptr<LearntAggrevate> LearntAggrevatePtr;

}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_ONLINE_POLICIES_LEARNT_AGGREVATE_H_ */
