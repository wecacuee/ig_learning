/* Copyright 2015 Sanjiban Choudhury
 * motion_length_feature_extractor.h
 *
 *  Created on: Jul 21, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_MOTION_LENGTH_FEATURE_EXTRACTOR_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_MOTION_LENGTH_FEATURE_EXTRACTOR_H_


#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class MotionLengthFeatureExtractor : public FeatureExtractor {
 public:
  MotionLengthFeatureExtractor() {}
  virtual ~MotionLengthFeatureExtractor() {}

  virtual void GetFeature(const State &state, const Action &action, std::vector<double> &feature) const;
 protected:
};
typedef boost::shared_ptr<MotionLengthFeatureExtractor> MotionLengthFeatureExtractorPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_FEATURE_EXTRACTOR_MOTION_LENGTH_FEATURE_EXTRACTOR_H_ */
