/* Copyright 2015 Sanjiban Choudhury
 * forward_training.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_FORWARD_TRAINING_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_FORWARD_TRAINING_H_

#include "ig_learning/state.h"
#include "ig_learning/cs_classification.h"
#include "ig_learning/state_transition.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/cost_function.h"
#include "ig_learning/clairvoyant_oracle.h"
#include "ig_learning/feature_extractor.h"

namespace ig_learning {

class ForwardTraining {
 public:
  struct Input {
    std::vector< std::pair<State, WorldMap> > train_dataset;
    std::vector< std::pair<State, WorldMap> > validation_dataset;
    unsigned int total_timesteps;
  };

  struct Output {
    std::vector <CSClassificationPtr> model_set;
    std::vector<double> train_error;
    std::vector<double> validation_error;
    std::vector< std::vector<double> > objective_trajectory_set;
  };

  class ActionSelection {
   public:
    enum mode { SUBSET = 0, ALL};

    ActionSelection()
    : mode_(mode::SUBSET),
      num_selection_(1) {}

    ActionSelection(mode m)
    : mode_(m),
      num_selection_(1) {}

    ActionSelection(mode m, unsigned int num_selection)
    : mode_(m),
      num_selection_(num_selection) {}

    ~ActionSelection(){}

    std::set<Action> SelectActions(const std::set<Action> &action_selection) const;
   private:
    unsigned int num_selection_;
    mode mode_;
  };

  struct Parameters {
    StateTransitionPtr state_belief_transition;
    ObjectiveFunctionPtr objective_fn;
    CostFunctionPtr cost_fn;
    double cost_budget;
    ClairvoyantOraclePtr oracle_fn;
    FeatureExtractorPtr feature_fn;
    CSClassificationPtr cs_class_fn;
    ActionSelection action_selection;
  };

  ForwardTraining(Parameters params)
  :params_(params) {}

  ~ForwardTraining() {}

  void Train(const Input &input, Output &output) const;
 private:
  Parameters params_;
};

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_IMITATION_LEARNING_FORWARD_TRAINING_H_ */
