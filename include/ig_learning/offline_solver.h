/* Copyright 2015 Sanjiban Choudhury
 * offline_solver.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_

#include "ig_learning/state.h"
#include "ig_learning/state/rssi_state.h"
#include "ig_learning/objective_functions/rssi_plus_pcl_voxel_obj_func.h"
#include "ig_learning/state_transition.h"
#include "ig_learning/objective_function.h"
#include "ig_learning/cost_function.h"

namespace ig_learning {

template <typename ObjectiveFunctionPtr>
struct OfflineSolverParamsT {
  ObjectiveFunctionPtr obj_fn;
  CostFunctionPtr cost_fn;
  StateTransitionPtr state_transition;
  typename WorldMap::Ptr world_map;
  typename State::Ptr state0;
  unsigned int budget;
  double cost_budget;
};

typedef OfflineSolverParamsT<ObjectiveFunctionPtr> OfflineSolverParams;


template <typename State>
struct OfflineSolverOutputT {
  std::vector<Action> action_sequence;
  typename State::Ptr state_final;
  std::vector<double> objective_trajectory;
  std::vector<double> cost_trajectory;
};

typedef OfflineSolverOutputT<State> OfflineSolverOutput;
typedef OfflineSolverOutputT<RSSIState> RSSIOfflineSolverOutput;

template <typename OfflineSolverParams, typename OfflineSolverOutput>
class OfflineSolverT {
 public:
  OfflineSolverT() {}
  virtual ~OfflineSolverT() {}

  virtual void Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const = 0;
};

template <typename OfflineSolverParams, typename OfflineSolverOutput>
using OfflineSolverPtrT = boost::shared_ptr<OfflineSolverT<OfflineSolverParams, OfflineSolverOutput> > ;

typedef OfflineSolverT<OfflineSolverParams, OfflineSolverOutput> OfflineSolver;
typedef OfflineSolverT<OfflineSolverParams, RSSIOfflineSolverOutput> RSSIOfflineSolver;
typedef OfflineSolverPtrT<OfflineSolverParams, OfflineSolverOutput> OfflineSolverPtr;
typedef OfflineSolverPtrT<OfflineSolverParams, RSSIOfflineSolverOutput> RSSIOfflineSolverPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVER_H_ */
