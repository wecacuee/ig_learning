/* Copyright 2015 Sanjiban Choudhury
 * state.h
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_

#include <stdint.h>
#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include "ig_active_reconstruction/view.hpp"
#include "ig_active_reconstruction/view_space.hpp"

#include "ig_active_reconstruction_octomap/octomap_basic_ray_ig_calculator.hpp"
#include "ig_active_reconstruction_octomap/octomap_std_pcl_input_point_xyz.hpp"
#include "ig_active_reconstruction_octomap/octomap_ig_tree_world_representation.hpp"
#include "ig_active_reconstruction_octomap/octomap_ray_occlusion_calculator.hpp"
#include "visualization_msgs/MarkerArray.h"


namespace ig_learning {


template<class TREE_TYPE>
class BeliefInterface : public ig_active_reconstruction::world_representation::octomap::WorldRepresentation<TREE_TYPE>::LinkedObject {
 public:
  typedef boost::shared_ptr< BeliefInterface<TREE_TYPE> > Ptr;
  typedef TREE_TYPE TreeType;

  struct Config {
    std::string world_frame_name;
  };

  BeliefInterface(Config config)
  : world_frame_name_(config.world_frame_name) {}

  virtual ~BeliefInterface() {}

  double GetCellCountInBBox(Eigen::Vector3d lb, Eigen::Vector3d ub) {
    unsigned int count = 0;
    for( typename TREE_TYPE::iterator it = this->link_.octree->begin(), end = this->link_.octree->end(); it!=end; ++it ){
      double size = it.getSize();
      double x = it.getX();
      double y = it.getY();
      double z = it.getZ();

      if (x < lb.x() || x > ub.x() || y < lb.y() || x > ub.y() || z < lb.z() || x > ub.z())
        continue;

      if( this->link_.octree->isNodeOccupied(*it) )
        count++;
    }
    return count;
  }

  visualization_msgs::MarkerArray GetMarkerArray() {
    visualization_msgs::MarkerArray occupiedNodesVis;
    // each array stores all cubes of a different size, one for each depth level:
    occupiedNodesVis.markers.resize(this->link_.octree->getTreeDepth()+1);

    std_msgs::ColorRGBA color;
    color.r = 0;
    color.g = 0;
    color.b = 1;
    color.a = 1;

    for( typename TREE_TYPE::iterator it = this->link_.octree->begin(), end = this->link_.octree->end(); it!=end; ++it )
    {
      double size = it.getSize();
      double x = it.getX();
      double y = it.getY();
      double z = it.getZ();

      geometry_msgs::Point cubeCenter;
      cubeCenter.x = x;
      cubeCenter.y = y;
      cubeCenter.z = z;
      unsigned idx = it.getDepth();

      if( this->link_.octree->isNodeOccupied(*it) )
      {
        double size = it.getSize();
        assert(idx < occupiedNodesVis.markers.size());

        occupiedNodesVis.markers[idx].points.push_back(cubeCenter);
      }
    }

    for (unsigned i= 0; i < occupiedNodesVis.markers.size(); ++i)
    {
      double size = this->link_.octree->getNodeSize(i);

      occupiedNodesVis.markers[i].header.frame_id = world_frame_name_;
      occupiedNodesVis.markers[i].header.stamp = ros::Time::now();
      occupiedNodesVis.markers[i].ns = "map";
      occupiedNodesVis.markers[i].id = i;
      occupiedNodesVis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
      occupiedNodesVis.markers[i].scale.x = size;
      occupiedNodesVis.markers[i].scale.y = size;
      occupiedNodesVis.markers[i].scale.z = size;
      occupiedNodesVis.markers[i].color = color;

      if (occupiedNodesVis.markers[i].points.size() > 0)
        occupiedNodesVis.markers[i].action = visualization_msgs::Marker::ADD;
      else
        occupiedNodesVis.markers[i].action = visualization_msgs::Marker::DELETE;
    }

    return occupiedNodesVis;
  }
 private:
  std::string world_frame_name_;
};

/**
 * \brief Action class
 * almost a POD except providing id
 */
struct Action {
  ig_active_reconstruction::views::View view;
  unsigned int id;
  bool operator < (const Action &other) const {return id < other.id;}
};

typedef pcl::PointCloud<pcl::PointXYZ> Observation;
typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation Belief;

/**
 * \brief POD representing state - actions taken so far, observations received so far and belief
 * It is overparameterized to aid in calculation of different functions
 */
struct State {
  typedef std::shared_ptr<State> Ptr;

  State()
  : action_set(),
    observation_set(),
    belief() {}

  State(const State &other)
  : action_set(other.action_set),
    observation_set(other.observation_set),
    belief(other.belief) {}

  State& operator=( const State& other ) {
    action_set = other.action_set;
    observation_set = other.observation_set;
    belief = other.belief;
    return *this;
  }


public:
  const std::vector<Action>& getAction_set() const;
  void setAction_set(const std::vector<Action> &value);
  void appendAction(const Action& action) {
      action_set.push_back(action);
  }
  void gatherActionSet(const std::vector<Action> &source, const std::vector<int>& indices);

  const std::vector<Observation>& getObservation_set() const;
  void appendObservation(const Observation& observation) {
      observation_set.push_back(observation);
  }
  void setObservation_set(const std::vector<Observation> &value);
  void gatherObservationSet(const std::vector<Observation> &source, const std::vector<int>& indices);

  const Belief& getBelief() const;
  void setBelief(const Belief &value);

  virtual bool InitializeState(const ros::NodeHandle&);
  visualization_msgs::MarkerArray VisualizeBelief() const;
  pcl::PointCloud<pcl::PointXYZ> VisualizeObservation() const;
  const Action& getLastAction() const {
    return action_set.back();
  }
  double GetCellCountInBBox(const Eigen::Vector3d& bbox_lb, const Eigen::Vector3d& bbox_ub) const;

  Eigen::Transform<double, 3, Eigen::Affine> addActionAndPointCloud(
          const pcl::PointCloud<pcl::PointXYZ>& cloud, const Action& action);

  typedef ig_active_reconstruction::world_representation::octomap::IgTreeWorldRepresentation::TreeType TreeType;
  void computeInformationGains(const Action &action, std::vector<double> &feature,
                               ig_active_reconstruction::world_representation::octomap::BasicRayIgCalculator<TreeType>::Config ig_calc_config,
                               ig_active_reconstruction::world_representation::octomap::InformationGain<TreeType>::Config ig_config);

  typedef ig_active_reconstruction::world_representation::octomap::StdPclInputPointXYZ<TreeType> StdPclInputPointXYZ;
  typedef StdPclInputPointXYZ::PclType PclType;
  typedef ig_active_reconstruction::world_representation::octomap::RayOcclusionCalculator<TreeType, PclType> RayOcclusionCalculator;
  void updateBeliefFromPointCloud(pcl::PointCloud<pcl::PointXYZ>& cloud,
                                  const Action& action,
                                  StdPclInputPointXYZ::Type::Config
                                  input_config,
                                  RayOcclusionCalculator::Options occlusion_config) {
    const auto sensor_to_world_transform = addActionAndPointCloud(cloud, action);
    typename StdPclInputPointXYZ::Ptr std_input = belief.getLinkedObj<ig_active_reconstruction::world_representation::octomap::StdPclInputPointXYZ>(input_config);
    std_input->setOcclusionCalculator<ig_active_reconstruction::world_representation::octomap::RayOcclusionCalculator> (occlusion_config);
    std_input->push(sensor_to_world_transform, cloud);
  }


protected:
  std::vector <Action> action_set;
  std::vector <Observation> observation_set;
  Belief belief;
};
typedef std::shared_ptr<State> StatePtr;

struct WorldMap {
public:
    typedef std::shared_ptr<WorldMap> Ptr;
    pcl::PointCloud<pcl::PointXYZ>& getModel_pcl();
    void setModel_pcl(const pcl::PointCloud<pcl::PointXYZ> &value);

    double getRes() const;
    void setRes(double value);

    std::map<unsigned int, pcl::PointCloud<pcl::PointXYZ> > getPcl_lookup() const;
    const pcl::PointCloud<pcl::PointXYZ>& getPointCloud(unsigned int idx) const;
    void setPcl_lookup(const std::map<unsigned int, pcl::PointCloud<pcl::PointXYZ> > &value);

    std::set<Action> getAction_set() const;
    void setAction_set(const std::set<Action> &value);

    virtual bool LoadWorldMap(std::string view_space_filename, std::string pcd_foldername, std::string model_filename, double res);

protected:
    pcl::PointCloud<pcl::PointXYZ> model_pcl;
    double res;
    std::map <unsigned int, pcl::PointCloud<pcl::PointXYZ> > pcl_lookup;
    std::set<Action> action_set;
};

typedef std::shared_ptr<WorldMap> WordMapPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_H_ */
