/* Copyright 2015 Sanjiban Choudhury
 * state_utils.h
 *
 *  Created on: Jul 7, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_UTILS_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_UTILS_H_

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

#include "ig_learning/state.h"

namespace ig_learning {
namespace state_utils {

void DisplayStateActionObservationSequence(const State &state, ros::Publisher &pub_pcl, tf::TransformBroadcaster &br, ros::Publisher &pub_poses, double dt);

Action GetRandomAction(const std::set<Action> &action_set);

bool LoadWorldMap(const ros::NodeHandle &n, typename WorldMap::Ptr world_map);

}
}


#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_UTILS_H_ */
