/* Copyright 2015 Sanjiban Choudhury
 * offline_solver_clairvoyant_oracle.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_OFFLINE_SOLVER_CLAIRVOYANT_ORACLE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_OFFLINE_SOLVER_CLAIRVOYANT_ORACLE_H_

#include "ig_learning/clairvoyant_oracle.h"
#include "ig_learning/offline_solver.h"

namespace ig_learning {

class OfflineSolverClairvoyantOracle : public ClairvoyantOracle {
 public:
  OfflineSolverClairvoyantOracle(const OfflineSolverPtr &solver, const OfflineSolverParams &input)
 : solver_(solver),
   input_(input) {}

  virtual ~OfflineSolverClairvoyantOracle() {}

  virtual double Value(const State &state, const Action &action, const WorldMap &world_map, unsigned int time_steps) const;
protected:
  OfflineSolverPtr solver_;
  OfflineSolverParams input_;
};

typedef boost::shared_ptr<OfflineSolverClairvoyantOracle> OfflineSolverClairvoyantOraclePtr;

}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_CLAIRVOYANT_ORACLES_OFFLINE_SOLVER_CLAIRVOYANT_ORACLE_H_ */
