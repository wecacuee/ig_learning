/* Copyright 2015 Sanjiban Choudhury
 * cost_function.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTION_H_

#include "ig_learning/state.h"

namespace ig_learning {

class CostFunction {
 public:
  CostFunction() {}
  virtual ~CostFunction() {}

  virtual double Cost(const State &state, const WorldMap &world_map) const = 0;
};

typedef boost::shared_ptr<CostFunction> CostFunctionPtr;

}





#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTION_H_ */
