/* Copyright 2015 Sanjiban Choudhury
 * lazy_greedy_nemhauser.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_LAZY_GREEDY_NEMHAUSER_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_LAZY_GREEDY_NEMHAUSER_H_

#include "ig_learning/offline_solver.h"

namespace ig_learning {

class LazyGreedyNemhauser : public OfflineSolver {
 public:
  LazyGreedyNemhauser() {}
  virtual ~LazyGreedyNemhauser() {}

  virtual void Solve(const OfflineSolverParams &input, OfflineSolverOutput &output) const;
};

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OFFLINE_SOLVERS_LAZY_GREEDY_NEMHAUSER_H_ */
