/* Copyright 2015 Sanjiban Choudhury
 * motion_cost_function.h
 *
 *  Created on: Jul 20, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_MOTION_COST_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_MOTION_COST_FUNCTION_H_

#include "ig_learning/cost_function.h"

namespace ig_learning {

class MotionCostFunction : public CostFunction {
 public:
  MotionCostFunction() {}

  virtual ~MotionCostFunction() {}

  virtual double Cost(const State &state, const WorldMap &world_map) const;
};


}




#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_MOTION_COST_FUNCTION_H_ */
