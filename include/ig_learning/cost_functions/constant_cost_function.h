/* Copyright 2015 Sanjiban Choudhury
 * constant_cost_function.h
 *
 *  Created on: Jul 21, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_CONSTANT_COST_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_CONSTANT_COST_FUNCTION_H_

#include "ig_learning/cost_function.h"

namespace ig_learning {

class ConstantCostFunction : public CostFunction {
 public:
  ConstantCostFunction(double val)
 : val_(val) {}

  virtual ~ConstantCostFunction() {}

  virtual double Cost(const State &state, const WorldMap &world_map) const {return val_;}

 protected:
  double val_;
};


}





#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_COST_FUNCTIONS_CONSTANT_COST_FUNCTION_H_ */
