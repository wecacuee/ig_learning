/* Copyright 2015 Sanjiban Choudhury
 * objective_function.h
 *
 *  Created on: Jun 29, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_

#include "ig_learning/state.h"

namespace ig_learning {

template <typename State, typename WorldMap>
 class ObjectiveFunctionT {
 public:
  ObjectiveFunctionT() {}
  virtual ~ObjectiveFunctionT() {}

  virtual double Value(const typename State::Ptr state, const typename WorldMap::Ptr world_map) const = 0;
};

template <typename State, typename WorldMap>
using ObjectiveFunctionPtrT = boost::shared_ptr<ObjectiveFunctionT<State, WorldMap> > ;

typedef ObjectiveFunctionT<State, WorldMap> ObjectiveFunction;
typedef ObjectiveFunctionPtrT<State, WorldMap> ObjectiveFunctionPtr;
}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_OBJECTIVE_FUNCTION_H_ */
