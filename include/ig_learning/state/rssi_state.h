#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_RSSI_STATE_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_RSSI_STATE_H_

#include <Eigen/Dense>

#include "ig_learning/state.h"

namespace ig_learning {

struct Gaussian {
  double value(const Eigen::Vector3d &x) const {
    auto diff = x - mu;
    return std::exp(- 0.5 * diff.transpose() * SigmaInv * diff)
      * std::sqrt(SigmaInv.determinant() / 2 * pi);
  }

  Gaussian& operator=(const Gaussian& other)
    {
     mu = other.mu;
     SigmaInv = other.SigmaInv;
    }


  Eigen::Vector3d& getMu(){ return mu; }
  Eigen::Matrix3d& getSigmaInv() { return SigmaInv; }

  void setMu(Eigen::Vector3d& x) { mu = x; }
  void setSigmaInv(Eigen::Matrix3d& x) { SigmaInv = x; }

private:
  const double pi = std::acos(-1);
  Eigen::Vector3d mu;
  Eigen::Matrix3d SigmaInv;
};

struct RSSIState : State {
  typedef std::shared_ptr<RSSIState> Ptr;
  std::vector<double>& getRSSIObservationSet() {
    return rssi_observation_set_;
  }
  void setRSSIObservationSet(std::vector<double> &rssi_obs_set) {
    rssi_observation_set_ = rssi_obs_set;
  }

  Gaussian& getRSSIBelief() {
    return rssi_belief_;
  }

  const Gaussian& getRSSIBelief() const {
    return rssi_belief_;
  }

  // void setRSSIBelief(Gaussian& rssi_belief) {
  //   rssi_belief_ = rssi_belief;
  // }
private:
  std::vector<double> rssi_observation_set_;
  Gaussian rssi_belief_;
};

typedef std::shared_ptr<RSSIState> RSSIStatePtr;

struct RSSIWorldMap : WorldMap {
  typedef std::shared_ptr<RSSIWorldMap> Ptr;
  std::vector<Eigen::Vector3d>& getRSSIPoses() {
    return rssi_poses_;
  }

  const std::vector<Eigen::Vector3d>& getRSSIPoses() const {
    return rssi_poses_;
  }

  void setRSSIPoses(std::vector<Eigen::Vector3d> &rssi_poses) {
    rssi_poses_ = rssi_poses;
  }

private:
  std::vector<Eigen::Vector3d> rssi_poses_;
};

typedef std::shared_ptr<RSSIWorldMap> RSSIWorldMapPtr;

}
#endif // IG_LEARNING_INCLUDE_IG_LEARNING_RSSI_STATE_H_
