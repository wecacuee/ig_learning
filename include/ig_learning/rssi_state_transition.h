/* Copyright 2015 Sanjiban Choudhury
 * state_transition.h
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_RSSI_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_RSSI_STATE_TRANSITION_H_

#include "ig_learning/rssi_state.h"
#include "ig_learning/state_transition.h"

namespace ig_learning {

typedef StateTransitionT<RSSIState, RSSIWorldMap> RSSIStateTransition;
typedef StateTransitionPtrT<RSSIState, RSSIWorldMap> RSSIStateTransitionPtr;

}


#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_ */
