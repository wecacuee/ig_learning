/* Copyright 2015 Sanjiban Choudhury
 * state_transition.h
 *
 *  Created on: Jun 28, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_

#include "ig_learning/state.h"

namespace ig_learning {

  template<typename State, typename WorldMap>
class StateTransitionT {
  public:
    StateTransitionT() {}

    virtual ~StateTransitionT() {}

    virtual bool UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const = 0;
  };

template <typename State, typename WorldMap>
using StateTransitionPtrT = boost::shared_ptr<StateTransitionT<State, WorldMap> >;

typedef StateTransitionT<State, WorldMap> StateTransition;
typedef StateTransitionPtrT<State, WorldMap> StateTransitionPtr;

}


#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_H_ */
