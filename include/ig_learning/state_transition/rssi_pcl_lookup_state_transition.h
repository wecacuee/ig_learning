/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_RSSI_PCL_LOOKUP_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_RSSI_PCL_LOOKUP_STATE_TRANSITION_H_

#include "ig_learning/state_transition.h"

namespace ig_learning {

  class RSSIPclLookupStateTransition : public StateTransition {
  public:
    RSSIPclLookupStateTransition() {}

    virtual ~RSSIPclLookupStateTransition() {}

    virtual bool UpdateState(const RSSIState &input, const Action &action, const RSSIWorldMap &world_map, RSSIState &output) const;

  };

  typedef boost::shared_ptr<RSSIPclLookupStateTransition> RSSIPclLookupStateTransitionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_ */
