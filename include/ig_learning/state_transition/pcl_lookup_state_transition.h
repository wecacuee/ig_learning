/* Copyright 2015 Sanjiban Choudhury
 * pcl_lookup_state_transition.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_

#include "ig_learning/state_transition.h"
#include "ig_learning/state/rssi_state.h"

namespace ig_learning {

template <typename State, typename WorldMap>
class PclLookupStateTransitionT : public StateTransitionT<State, WorldMap> {
public:
  PclLookupStateTransitionT() {}

  virtual ~PclLookupStateTransitionT() {}

  virtual bool UpdateState(const State &input, const Action &action, const WorldMap &world_map, State &output) const;

};

template <typename State, typename WorldMap>
using PclLookupStateTransitionPtrT = boost::shared_ptr<PclLookupStateTransitionT<State, WorldMap> > ;

typedef PclLookupStateTransitionT<State, WorldMap> PclLookupStateTransition;
typedef PclLookupStateTransitionT<RSSIState, RSSIWorldMap> RSSIPclLookupStateTransition;
typedef PclLookupStateTransitionPtrT<State, WorldMap> PclLookupStateTransitionPtr;
typedef PclLookupStateTransitionPtrT<RSSIState, RSSIWorldMap> RSSIPclLookupStateTransitionPtr;

}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_STATE_TRANSITION_PCL_LOOKUP_STATE_TRANSITION_H_ */
