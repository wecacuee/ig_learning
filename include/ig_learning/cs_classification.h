/* Copyright 2015 Sanjiban Choudhury
 * cs_classification.h
 *
 *  Created on: Jul 8, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_H_
#define IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_H_

#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <vector>

namespace ig_learning {

class CSClassification {
 public:
  CSClassification() {}
  virtual ~CSClassification() {}

  virtual bool Train(const std::vector<Eigen::MatrixXd> &feature_table, const std::vector<Eigen::VectorXd> &qval_table) = 0;

  virtual unsigned int Predict(const Eigen::MatrixXd &feature) = 0;

  virtual bool Save(const std::string &filename) = 0;

  virtual bool Load(const std::string &filename) = 0;

  virtual CSClassification *Clone() const = 0; // Factory Method

};

typedef boost::shared_ptr<CSClassification> CSClassificationPtr;


}



#endif /* IG_LEARNING_INCLUDE_IG_LEARNING_CS_CLASSIFICATION_H_ */
